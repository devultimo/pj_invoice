﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(pj_Invoice.Startup))]
namespace pj_Invoice
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
