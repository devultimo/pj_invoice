namespace pj_Invoice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitModels1 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.StatusInvoices", newName: "InvoiceStatus");
            RenameTable(name: "dbo.TipeInvoices", newName: "InvoiceTypes");
            DropForeignKey("dbo.Ticketings", "StatusTicketing_Id", "dbo.StatusTicketings");
            DropForeignKey("dbo.Ticketings", "TipeTicketing_Id", "dbo.TipeTicketings");
            DropIndex("dbo.Ticketings", new[] { "StatusTicketing_Id" });
            DropIndex("dbo.Ticketings", new[] { "TipeTicketing_Id" });
            RenameColumn(table: "dbo.Invoices", name: "StatusInvoice_Id", newName: "InvoiceStatus_Id");
            RenameColumn(table: "dbo.Invoices", name: "TipeInvoice_Id", newName: "InvoiceType_Id");
            RenameIndex(table: "dbo.Invoices", name: "IX_StatusInvoice_Id", newName: "IX_InvoiceStatus_Id");
            RenameIndex(table: "dbo.Invoices", name: "IX_TipeInvoice_Id", newName: "IX_InvoiceType_Id");
            CreateTable(
                "dbo.TicketingStatus",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Status = c.String(nullable: false, maxLength: 25),
                        Information = c.String(),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketingTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Type = c.String(nullable: false, maxLength: 25),
                        Information = c.String(),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Customers", "Name", c => c.String(nullable: false, maxLength: 25));
            AddColumn("dbo.Customers", "Address", c => c.String(nullable: false, maxLength: 255));
            AddColumn("dbo.Customers", "PhoneNumber", c => c.String(nullable: false, maxLength: 13));
            AddColumn("dbo.Customers", "Gender", c => c.String(nullable: false, maxLength: 1));
            AddColumn("dbo.Invoices", "TypeId", c => c.Guid(nullable: false));
            AddColumn("dbo.Invoices", "SentDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Invoices", "TotalBill", c => c.Double(nullable: false));
            AddColumn("dbo.Invoices", "Tax", c => c.Double(nullable: false));
            AddColumn("dbo.Invoices", "Discount", c => c.Double(nullable: false));
            AddColumn("dbo.Invoices", "TotalPayment", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Domain", c => c.String(maxLength: 25));
            AddColumn("dbo.Projects", "ProjectType", c => c.String(maxLength: 25));
            AddColumn("dbo.Projects", "StartDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Projects", "EndDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.InvoiceStatus", "Status", c => c.String(nullable: false, maxLength: 25));
            AddColumn("dbo.InvoiceTypes", "Type", c => c.String(nullable: false, maxLength: 25));
            AddColumn("dbo.Ticketings", "Title", c => c.String(nullable: false, maxLength: 25));
            AddColumn("dbo.Ticketings", "Notes", c => c.String(nullable: false));
            AddColumn("dbo.Ticketings", "SentDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Ticketings", "ProcessDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Ticketings", "Priority", c => c.String());
            AddColumn("dbo.Ticketings", "PersonInCharge", c => c.String(nullable: false, maxLength: 25));
            AddColumn("dbo.Ticketings", "TypeId", c => c.Guid(nullable: false));
            AddColumn("dbo.Ticketings", "TicketingStatus_Id", c => c.Guid());
            AddColumn("dbo.Ticketings", "TicketingType_Id", c => c.Guid());
            CreateIndex("dbo.Ticketings", "TicketingStatus_Id");
            CreateIndex("dbo.Ticketings", "TicketingType_Id");
            AddForeignKey("dbo.Ticketings", "TicketingStatus_Id", "dbo.TicketingStatus", "Id");
            AddForeignKey("dbo.Ticketings", "TicketingType_Id", "dbo.TicketingTypes", "Id");
            DropColumn("dbo.Customers", "Nama");
            DropColumn("dbo.Customers", "Alamat");
            DropColumn("dbo.Customers", "Nohp");
            DropColumn("dbo.Customers", "JenisKelamin");
            DropColumn("dbo.Invoices", "TipeId");
            DropColumn("dbo.Invoices", "TanggalKirim");
            DropColumn("dbo.Invoices", "JumlahTagihan");
            DropColumn("dbo.Invoices", "Diskon");
            DropColumn("dbo.Invoices", "JumlahPembayaran");
            DropColumn("dbo.Projects", "NamaDomain");
            DropColumn("dbo.Projects", "JenisProject");
            DropColumn("dbo.Projects", "TanggalMulai");
            DropColumn("dbo.Projects", "TanggalBerakhir");
            DropColumn("dbo.InvoiceStatus", "NamaStatus");
            DropColumn("dbo.InvoiceTypes", "NamaTipe");
            DropColumn("dbo.Ticketings", "Judul");
            DropColumn("dbo.Ticketings", "Catatan");
            DropColumn("dbo.Ticketings", "TanggalKirim");
            DropColumn("dbo.Ticketings", "TanggalProses");
            DropColumn("dbo.Ticketings", "Prioritas");
            DropColumn("dbo.Ticketings", "penanggungJawab");
            DropColumn("dbo.Ticketings", "TipeId");
            DropColumn("dbo.Ticketings", "StatusTicketing_Id");
            DropColumn("dbo.Ticketings", "TipeTicketing_Id");
            DropTable("dbo.StatusTicketings");
            DropTable("dbo.TipeTicketings");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TipeTicketings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        NamaTipe = c.String(nullable: false, maxLength: 25),
                        Keterangan = c.String(),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StatusTicketings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        NamaStatus = c.String(nullable: false, maxLength: 25),
                        Keterangan = c.String(),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Ticketings", "TipeTicketing_Id", c => c.Guid());
            AddColumn("dbo.Ticketings", "StatusTicketing_Id", c => c.Guid());
            AddColumn("dbo.Ticketings", "TipeId", c => c.Guid(nullable: false));
            AddColumn("dbo.Ticketings", "penanggungJawab", c => c.String(nullable: false, maxLength: 25));
            AddColumn("dbo.Ticketings", "Prioritas", c => c.String());
            AddColumn("dbo.Ticketings", "TanggalProses", c => c.DateTime(nullable: false));
            AddColumn("dbo.Ticketings", "TanggalKirim", c => c.DateTime(nullable: false));
            AddColumn("dbo.Ticketings", "Catatan", c => c.String(nullable: false));
            AddColumn("dbo.Ticketings", "Judul", c => c.String(nullable: false, maxLength: 25));
            AddColumn("dbo.InvoiceTypes", "NamaTipe", c => c.String(nullable: false, maxLength: 25));
            AddColumn("dbo.InvoiceStatus", "NamaStatus", c => c.String(nullable: false, maxLength: 25));
            AddColumn("dbo.Projects", "TanggalBerakhir", c => c.DateTime(nullable: false));
            AddColumn("dbo.Projects", "TanggalMulai", c => c.DateTime(nullable: false));
            AddColumn("dbo.Projects", "JenisProject", c => c.String(maxLength: 25));
            AddColumn("dbo.Projects", "NamaDomain", c => c.String(maxLength: 25));
            AddColumn("dbo.Invoices", "JumlahPembayaran", c => c.Double(nullable: false));
            AddColumn("dbo.Invoices", "Diskon", c => c.Double(nullable: false));
            AddColumn("dbo.Invoices", "JumlahTagihan", c => c.Double(nullable: false));
            AddColumn("dbo.Invoices", "TanggalKirim", c => c.DateTime(nullable: false));
            AddColumn("dbo.Invoices", "TipeId", c => c.Guid(nullable: false));
            AddColumn("dbo.Customers", "JenisKelamin", c => c.String(nullable: false, maxLength: 1));
            AddColumn("dbo.Customers", "Nohp", c => c.String(nullable: false, maxLength: 13));
            AddColumn("dbo.Customers", "Alamat", c => c.String(nullable: false, maxLength: 255));
            AddColumn("dbo.Customers", "Nama", c => c.String(nullable: false, maxLength: 25));
            DropForeignKey("dbo.Ticketings", "TicketingType_Id", "dbo.TicketingTypes");
            DropForeignKey("dbo.Ticketings", "TicketingStatus_Id", "dbo.TicketingStatus");
            DropIndex("dbo.Ticketings", new[] { "TicketingType_Id" });
            DropIndex("dbo.Ticketings", new[] { "TicketingStatus_Id" });
            DropColumn("dbo.Ticketings", "TicketingType_Id");
            DropColumn("dbo.Ticketings", "TicketingStatus_Id");
            DropColumn("dbo.Ticketings", "TypeId");
            DropColumn("dbo.Ticketings", "PersonInCharge");
            DropColumn("dbo.Ticketings", "Priority");
            DropColumn("dbo.Ticketings", "ProcessDate");
            DropColumn("dbo.Ticketings", "SentDate");
            DropColumn("dbo.Ticketings", "Notes");
            DropColumn("dbo.Ticketings", "Title");
            DropColumn("dbo.InvoiceTypes", "Type");
            DropColumn("dbo.InvoiceStatus", "Status");
            DropColumn("dbo.Projects", "EndDate");
            DropColumn("dbo.Projects", "StartDate");
            DropColumn("dbo.Projects", "ProjectType");
            DropColumn("dbo.Projects", "Domain");
            DropColumn("dbo.Invoices", "TotalPayment");
            DropColumn("dbo.Invoices", "Discount");
            DropColumn("dbo.Invoices", "Tax");
            DropColumn("dbo.Invoices", "TotalBill");
            DropColumn("dbo.Invoices", "SentDate");
            DropColumn("dbo.Invoices", "TypeId");
            DropColumn("dbo.Customers", "Gender");
            DropColumn("dbo.Customers", "PhoneNumber");
            DropColumn("dbo.Customers", "Address");
            DropColumn("dbo.Customers", "Name");
            DropTable("dbo.TicketingTypes");
            DropTable("dbo.TicketingStatus");
            RenameIndex(table: "dbo.Invoices", name: "IX_InvoiceType_Id", newName: "IX_TipeInvoice_Id");
            RenameIndex(table: "dbo.Invoices", name: "IX_InvoiceStatus_Id", newName: "IX_StatusInvoice_Id");
            RenameColumn(table: "dbo.Invoices", name: "InvoiceType_Id", newName: "TipeInvoice_Id");
            RenameColumn(table: "dbo.Invoices", name: "InvoiceStatus_Id", newName: "StatusInvoice_Id");
            CreateIndex("dbo.Ticketings", "TipeTicketing_Id");
            CreateIndex("dbo.Ticketings", "StatusTicketing_Id");
            AddForeignKey("dbo.Ticketings", "TipeTicketing_Id", "dbo.TipeTicketings", "Id");
            AddForeignKey("dbo.Ticketings", "StatusTicketing_Id", "dbo.StatusTicketings", "Id");
            RenameTable(name: "dbo.InvoiceTypes", newName: "TipeInvoices");
            RenameTable(name: "dbo.InvoiceStatus", newName: "StatusInvoices");
        }
    }
}
