namespace pj_Invoice.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nama = c.String(nullable: false, maxLength: 25),
                        Alamat = c.String(nullable: false, maxLength: 255),
                        Nohp = c.String(nullable: false, maxLength: 13),
                        Email = c.String(nullable: false, maxLength: 25),
                        JenisKelamin = c.String(nullable: false, maxLength: 1),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProjectId = c.Guid(nullable: false),
                        TipeId = c.Guid(nullable: false),
                        StatusId = c.Guid(nullable: false),
                        TanggalKirim = c.DateTime(nullable: false),
                        DueDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        JumlahTagihan = c.Double(nullable: false),
                        Diskon = c.Double(nullable: false),
                        JumlahPembayaran = c.Double(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        StatusInvoice_Id = c.Guid(),
                        TipeInvoice_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .ForeignKey("dbo.StatusInvoices", t => t.StatusInvoice_Id)
                .ForeignKey("dbo.TipeInvoices", t => t.TipeInvoice_Id)
                .Index(t => t.ProjectId)
                .Index(t => t.StatusInvoice_Id)
                .Index(t => t.TipeInvoice_Id);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CustomerId = c.Guid(nullable: false),
                        NamaDomain = c.String(maxLength: 25),
                        JenisProject = c.String(maxLength: 25),
                        TanggalMulai = c.DateTime(nullable: false),
                        TanggalBerakhir = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.StatusInvoices",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        NamaStatus = c.String(nullable: false, maxLength: 25),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TipeInvoices",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        NamaTipe = c.String(nullable: false, maxLength: 25),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.StatusTicketings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        NamaStatus = c.String(nullable: false, maxLength: 25),
                        Keterangan = c.String(),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Ticketings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CustomerId = c.Guid(nullable: false),
                        Judul = c.String(nullable: false, maxLength: 25),
                        Catatan = c.String(nullable: false),
                        TanggalKirim = c.DateTime(nullable: false),
                        TanggalProses = c.DateTime(nullable: false),
                        Prioritas = c.String(),
                        penanggungJawab = c.String(nullable: false, maxLength: 25),
                        StatusId = c.Guid(nullable: false),
                        TipeId = c.Guid(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        StatusTicketing_Id = c.Guid(),
                        TipeTicketing_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .ForeignKey("dbo.StatusTicketings", t => t.StatusTicketing_Id)
                .ForeignKey("dbo.TipeTicketings", t => t.TipeTicketing_Id)
                .Index(t => t.CustomerId)
                .Index(t => t.StatusTicketing_Id)
                .Index(t => t.TipeTicketing_Id);
            
            CreateTable(
                "dbo.TipeTicketings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        NamaTipe = c.String(nullable: false, maxLength: 25),
                        Keterangan = c.String(),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Ticketings", "TipeTicketing_Id", "dbo.TipeTicketings");
            DropForeignKey("dbo.Ticketings", "StatusTicketing_Id", "dbo.StatusTicketings");
            DropForeignKey("dbo.Ticketings", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Invoices", "TipeInvoice_Id", "dbo.TipeInvoices");
            DropForeignKey("dbo.Invoices", "StatusInvoice_Id", "dbo.StatusInvoices");
            DropForeignKey("dbo.Invoices", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Projects", "CustomerId", "dbo.Customers");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Ticketings", new[] { "TipeTicketing_Id" });
            DropIndex("dbo.Ticketings", new[] { "StatusTicketing_Id" });
            DropIndex("dbo.Ticketings", new[] { "CustomerId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Projects", new[] { "CustomerId" });
            DropIndex("dbo.Invoices", new[] { "TipeInvoice_Id" });
            DropIndex("dbo.Invoices", new[] { "StatusInvoice_Id" });
            DropIndex("dbo.Invoices", new[] { "ProjectId" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.TipeTicketings");
            DropTable("dbo.Ticketings");
            DropTable("dbo.StatusTicketings");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.TipeInvoices");
            DropTable("dbo.StatusInvoices");
            DropTable("dbo.Projects");
            DropTable("dbo.Invoices");
            DropTable("dbo.Customers");
        }
    }
}
