﻿using System.ComponentModel.DataAnnotations;

namespace pj_Invoice.Models.Schema.Master
{
    public class TicketingType: BaseEntity
    {
        [Required]
        [StringLength(25)]
        public string Type { get; set; }

        public string Information { get; set; }
    }
}