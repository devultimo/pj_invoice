﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pj_Invoice.Models.Schema.Master
{
    public class TicketingStatus: BaseEntity
    {
        [Required]
        [StringLength(25)]
        public string Status { get; set; }
        public string Information { get; set; }
    }
}