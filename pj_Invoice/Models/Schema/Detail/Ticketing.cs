﻿using pj_Invoice.Models.Schema.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace pj_Invoice.Models.Schema.Detail
{
    public class Ticketing:BaseEntity
    {
        public Guid CustomerId { get; set; }

        [Required]
        [StringLength(25)]
        public string Title { get; set; }

        [Required]
        public string Notes { get; set; }

        [Required]
        public DateTime SentDate { get; set; }

        public DateTime ProcessDate { get; set; }

        public string Priority { get; set; }

        [Required]
        [StringLength(25)]
        public string PersonInCharge { get; set; }

        public Guid StatusId { get; set; }

        public Guid TypeId { get; set; }
        public virtual TicketingType TicketingType { get; set; }
        public virtual TicketingStatus TicketingStatus { get; set; }
        public virtual Customer Customer { get; set; }


    }
}