﻿using pj_Invoice.Models.Schema.Detail;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pj_Invoice.Models.Schema.Master
{
    public class Invoice : BaseEntity
    {
        public Guid ProjectId { get; set; }

        public Guid TypeId { get; set; }

        [Required]        
        public Guid StatusId { get; set; }

        [Required]
        public DateTime SentDate { get; set; }

        [Required]
        public DateTime DueDate { get; set; }

        public DateTime PaymentDate { get; set; }

        [Required]
        public double TotalBill { get; set; }
        public double Tax { get; set; }
        public double Discount { get; set; }

        public double TotalPayment { get; set; }

        public virtual Project Project { get; set; }
        public virtual InvoiceType InvoiceType { get; set; }
        public virtual InvoiceStatus InvoiceStatus { get; set; }
    }
}