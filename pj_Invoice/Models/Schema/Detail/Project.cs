﻿using pj_Invoice.Models.Schema.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pj_Invoice.Models.Schema.Detail
{
    public class Project : BaseEntity
    {
        public Guid CustomerId { get; set; }
        
        [StringLength(25)]
        public string Domain { get; set; }

        [StringLength(25)]
        public string ProjectType { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        public virtual Customer Customer { get; set; }



    }
}